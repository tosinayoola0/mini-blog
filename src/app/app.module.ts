import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import {SharedModule } from './shared/shared.module';
import {EditorModule } from './editor/editor.module';
import {CoreModule } from './core/core.module';
import {AuthModule } from './auth/auth.module';
import {HomeModule } from './home/home.module';
import {ArticleModule } from './article/article.module';
import {MaterialModule } from './material/material.module';


@NgModule({
  declarations: [
    AppComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    EditorModule,
    ArticleModule,
    HttpClientModule,
    ReactiveFormsModule,
    MaterialModule,
    FormsModule,
    SharedModule,
    HomeModule,
    AuthModule,
    CoreModule,

  ],

  providers: [],
  bootstrap: [AppComponent],
  exports: [],
})
export class AppModule { }
