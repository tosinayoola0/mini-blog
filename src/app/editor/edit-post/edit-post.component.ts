import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CoreService } from '../../core/service/core.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {Article } from '../../models/articleData';


@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.css']
})
export class EditPostComponent implements OnInit {
  editArticleForm: FormGroup;
  article: any = {};


  constructor(
    private coreService: CoreService,
    private fb: FormBuilder,
    public router: Router,
    private route: ActivatedRoute,

  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const newLocal = 'slug';
      this.coreService.editArticle(params[newLocal]).subscribe(
      (res: any) => {
          this.article = res.article;
          this.editArticleForm.patchValue(this.article);
      }
    );
  });

    this.editArticleForm = this.fb.group({
      title: [
        '', [
          Validators.required
        ]
      ],
      body: [
        '', [
          Validators.required
        ]
      ],
      description: [
        '', [
          Validators.required
        ]
      ]
    });
  }


  updateArticle(article: Article){
    this.route.params.subscribe( params => {
      this.coreService.updateArticle(params.slug, article);
      this.router.navigate(['/index']);
    });
  }

}
