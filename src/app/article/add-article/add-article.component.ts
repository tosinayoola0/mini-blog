import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import {CoreService } from '../../core/service/core.service';
import {NewArticle, Article } from '../../models/articleData';
import {AuthService } from '../../core/service/auth.service';


@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {
  articleForm: FormGroup;
  action: string;
  article: Article;
  articles: Article[];

  constructor(
    private coreService: CoreService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    ) { }

  ngOnInit(): void {
    this.action = this.article ? 'new' : 'update';
    this.articleForm =  this.formBuilder.group({
      title: [
        '', [
          Validators.required
        ]
      ],

      body: [
        '', [
          Validators.required
        ]
      ],

      description: [
        '', [
          Validators.required
        ]
      ],

      tags: [
        '', [
          Validators.required
        ]
      ]
    });
  }

  CreateArticle() {
    this.coreService.CreateArticle(this.articleForm.value)
      .subscribe(
        artciles => this.articles.push()
      );
  }

}

