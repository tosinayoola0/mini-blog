import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { MaterialModule } from '../material/material.module';
import { AddArticleComponent } from './add-article/add-article.component';

const Components = [
  AddArticleComponent,
];

@NgModule({
  declarations: [Components],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],

  exports: [Components]
})
export class ArticleModule { }

