export interface Article{
  id: string;
  slug?: string;
  title: string;
  description?: string;
  tagList?: Tags;
  body: string;
  author?: AuthorDetail;

}

export interface  NewArticle{
  title: string;
  description: string;
  tag?: [string];
  body: string;
}


export interface FavouriteArticles{
  articleList: [Article];
}

export interface AuthorDetail{
  id?: number;
  username?: string;
  bio?: string;
  image?: string;
  following?: boolean;
}

export interface Tags{
  tag: [string];
}
