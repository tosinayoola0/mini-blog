//import {Deserializable} from './modelDeserializer';

export interface AuthorDetail{
  id?: number;
  username?: string;
  bio?: string;
  image?: string;
  following?: boolean;


}
/**
export class AuthorDetail implements Deserializable{
  id?: number;
  username?: string;
  bio?: string;
  image?: string;
  following?: boolean;

  deserialize(input: any): this{
    Object.assign(this, input);
    return this;
  }


}
*/
