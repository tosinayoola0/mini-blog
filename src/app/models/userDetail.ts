
export interface SignupDetail{
  id?: number;
  username: string;
  email: string;
  password: string;

}

export interface LoginDetail{
  email: string;
  password: string;
}



