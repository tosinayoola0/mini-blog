import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {Article, FavouriteArticles, Tags } from '../../models/articleData';
import {AuthService } from '../../core/service/auth.service';
import {CoreService } from '../../core/service/core.service';


@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {
  favoritePost: FavouriteArticles[];
  @Input() article: Article;
  @Output() deleteArticle: EventEmitter<Article> = new EventEmitter();
  @Output() addFavoritePost: EventEmitter<Article> =  new EventEmitter();
  tags: Tags[];


  constructor(
    public authService: AuthService,
    public coreService: CoreService,
    ) { }
  currentUserUsername: string;

  ngOnInit(): void {
    this.currentUserUsername = localStorage.getItem('username');
    this.getTags();
}

  deletePost(article: Article){
    this.deleteArticle.emit(article);
  }

  favoriteArticle(post){
    this.addFavoritePost.emit(post);
  }

  getTags(){
    return this.coreService.getTags().subscribe(tags => this.tags = tags );
  }

}
