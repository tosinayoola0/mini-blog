import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxPaginationModule} from 'ngx-pagination';
import {AppRoutingModule } from '../app-routing.module';
import {MaterialModule } from '../material/material.module';
import {HeaderComponent } from './nav/header/header.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleListComponent } from './article-list/article-list.component';
import { ArticleDetailComponent } from './article-detail/article-detail.component';
import { FavoriteArticlesComponent } from './favorite-articles/favorite-articles.component';
import { AuthorPostComponent } from './author-post/author-post.component';

const Components = [
        ArticlesComponent,
        ArticleListComponent,
        ArticleDetailComponent,
        HeaderComponent,
        FavoriteArticlesComponent,
        AuthorPostComponent
];

@NgModule({
  declarations: [Components,  ],

  imports: [
    CommonModule,
    AppRoutingModule,
    MaterialModule,
    NgxPaginationModule,

  ],

  exports : [Components ]
})
export class HomeModule { }
