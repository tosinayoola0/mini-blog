import { Component, OnInit } from '@angular/core';
import {ActivatedRoute } from '@angular/router';
import {Article } from '../../models/articleData';
import {CoreService } from '../../core/service/core.service';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {
  article: Article;

  constructor(
        private route: ActivatedRoute,
        private coreService: CoreService
  ) { }

  ngOnInit(): void {
  this.getArticle();
  }


  getArticle(){
    const slug = this.route.snapshot.paramMap.get('slug');
    this.coreService.getArticle(slug)
    .subscribe(article => this.article = article);
  }

}
