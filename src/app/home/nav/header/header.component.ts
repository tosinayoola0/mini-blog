import { Component, OnInit, ViewEncapsulation,
  AfterViewInit, Output, HostListener,  EventEmitter } from '@angular/core';
import {Router } from '@angular/router';
import {AuthService } from '../../../core/service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None,

})
export class HeaderComponent implements OnInit {

  constructor(
    public authService: AuthService) { }

  ngOnInit(): void { }

  logout(){
    this.authService.logOut();
  }

}
