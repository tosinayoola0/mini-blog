import { Component, OnInit } from '@angular/core';
import {CoreService } from '../../core/service/core.service';
import {AuthService } from '../../core/service/auth.service';
import {Article } from '../../models/articleData';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-author-post',
  templateUrl: './author-post.component.html',
  styleUrls: ['./author-post.component.css']
})
export class AuthorPostComponent implements OnInit {
  articles: Article[];
  currentUserUsername: string;
  article: Article;

  constructor(
    public authService: AuthService,
    private coreService: CoreService,
  ) { }


  ngOnInit(): void {
    this.articlesByAuthor();
    this.currentUserUsername = localStorage.getItem('username');
  }

  articlesByAuthor() {
    this.coreService.getArticles().subscribe(articles => this.articles = articles);

  }

  deletePost(article: Article){
    this.articles = this.articles.filter(post => article.slug !== post.slug);
    this.coreService.deleteArticle(article).subscribe();
  }
}
