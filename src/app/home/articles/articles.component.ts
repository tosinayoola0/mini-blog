import { Component, OnInit } from '@angular/core';
import {CoreService } from '../../core/service/core.service';
import {Article, FavouriteArticles, Tags } from '../../models/articleData';
import {ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})

export class ArticlesComponent implements OnInit {
  articles: Article[];
  article: Article;
  tags: Tags[];
  listOfArticles: FavouriteArticles[];
  p = 1;

  constructor(
    private coreService: CoreService,
    private route: ActivatedRoute,

    ) { }

  ngOnInit(): void {
    this.getArticles();
  }

  getArticles(){
    return this.coreService.getArticles().subscribe(article => this.articles = article);
  }

  deletePost(article: Article){
    this.articles = this.articles.filter(post => article.slug !== post.slug);
    this.coreService.deleteArticle(article).subscribe();
  }

  addFavoriteArticle(article){
    return this.coreService.favoriteArticle(article)
    .subscribe(
      favoriteArticle => this.listOfArticles.push(article)
    );
  }



}
