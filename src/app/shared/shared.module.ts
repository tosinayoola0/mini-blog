import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {GuardGuard } from './guard.guard';
import {MaterialModule } from '../material/material.module';
import {AuthInterceptor } from '../core/interceptor/auth.inceptor';


const Components = [

];

@NgModule({
  declarations: [Components],

  imports: [
    CommonModule,
    HttpClientModule,
    MaterialModule,
  ],

  exports: [Components],

  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    GuardGuard
  ],
})
export class SharedModule { }
