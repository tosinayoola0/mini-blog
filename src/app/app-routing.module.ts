import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {GuardGuard } from './shared/guard.guard';
import { LoginComponent } from './auth/login/login.component';
import {SignupComponent } from './auth/signup/signup.component';
import {ArticlesComponent } from './home/articles/articles.component';
import {EditPostComponent } from './editor/edit-post/edit-post.component';
import {AuthorPostComponent } from './home/author-post/author-post.component';
import {AddArticleComponent } from './article/add-article/add-article.component';
import { ArticleDetailComponent } from './home/article-detail/article-detail.component';
import {FavoriteArticlesComponent } from './home/favorite-articles/favorite-articles.component';


const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'index', component: ArticlesComponent},
  {path: 'register', component: SignupComponent},
  {path: 'editor', component: AddArticleComponent},
  {path: 'my-post', component: AuthorPostComponent},
  {path: '', redirectTo: '/index', pathMatch: 'full'},
  {path: 'article/:slug', component: ArticleDetailComponent},
  {path: 'edit-article/:slug', component: EditPostComponent},
  {path: 'my-articles', component: FavoriteArticlesComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
