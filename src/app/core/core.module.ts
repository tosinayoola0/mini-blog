import { NgModule } from '@angular/core';
import { AppRoutingModule } from '../app-routing.module';
import {SharedModule } from '../shared/shared.module';
import { CommonModule } from '@angular/common';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import {MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [NavBarComponent],

  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    AppRoutingModule,
  ],

  exports: [NavBarComponent ]
})
export class CoreModule { }

