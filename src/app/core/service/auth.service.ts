import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {environment } from '../../../environments/environment';
import {SignupDetail, LoginDetail} from '../../models/userDetail';
import { Observable, throwError } from 'rxjs';
import {map, catchError, } from 'rxjs/operators';
import {Router, ActivatedRoute } from '@angular/router';
import { Profile } from '../../profile/profile/data';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
                private httpClient: HttpClient,
                private router: Router,
                private activateRoute: ActivatedRoute,
  ) {}

  url = environment.baseUrl;

  httpOptions = {
    headers : new HttpHeaders({
      'Content-type': 'application/json'
    })
  };

  userSignUp(user: SignupDetail): Observable<SignupDetail>{
     const signupUrl = `${this.url}/users`;
     return this.httpClient.post<SignupDetail>(signupUrl, {user} ).pipe(
       map(res => res),
       catchError(this.handleError)
     );
  }

  handleError(error: HttpErrorResponse){
    let msg = '';
    if (error.error instanceof ErrorEvent){
      msg = error.message;
    } else {
      msg = `Error message: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }


  getToken(){
     return localStorage.getItem('access_token');
  }

  get isLoggined(): boolean {
    const userToken = localStorage.getItem('access_token');
    return ( userToken !== null) ? true : false;
  }

  userLogin(user: LoginDetail){
    const loginUrl = `${this.url}/users/login`;
    return this.httpClient.post<any>(loginUrl, {user})
  .subscribe((res: any) => {
      localStorage.setItem('access_token', res.user.token);
      localStorage.setItem('username', res.user.username);
      console.log(res.user.token);
      this.router.navigate(['index']);
    });
  }

  getProfile(username): Observable<any>{
    const profileUrl = `${this.url}/profile/username`;
    return this.httpClient.get(profileUrl).pipe(
      map(res => res),
      catchError(this.handleError)
    );
  }
  userProfile(profile: Profile){
  }

  logOut(){
    const removeToken = localStorage.removeItem('access_token');
    if ( removeToken == null){
      this.router.navigate(['login']);
    }
  }

  createProfile(profile: Profile, username: string): Observable<Profile>{
    const url = `${this.url}/profiles/${username}/follow`;
    return this.httpClient.post<Profile>(url, {profile}, this.httpOptions);
  }


}
