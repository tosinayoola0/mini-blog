import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError } from 'rxjs';
import {tap, catchError, map} from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { NewArticle, Article, FavouriteArticles, Tags } from '../../models/articleData';

@Injectable({
  providedIn: 'root'
})
export class CoreService {
  url = environment.baseUrl;
  articles: Article[];
  FavoritteArticle: FavouriteArticles[];

  httpOption = {
    headers: new HttpHeaders({'Content-type': 'application-json'})
  };

  constructor(private httpClient: HttpClient) { }

  private extraData(res: any){
    return res.articles || [];
  }

  getArticles(): Observable<Article[]> {
    const articlesUrl = `${this.url}/articles`;
    return this.httpClient.get<Article[]>(articlesUrl).pipe(
      map(this.extraData));
  }


  CreateArticle(article: NewArticle): Observable<any>{
    const articleUrl = `${this.url}/articles`;
    return this.httpClient.post(articleUrl, {article}, this.httpOption)
    .pipe(
      tap(_ => console.log('post successfully edited'))
    );
  }

  updateArticle(slug: string, article: Article): Observable<any>{
    const updateUrl = `${this.url}/article/${slug}`;
    return this.httpClient.put(updateUrl, article, this.httpOption).pipe(
      map(res => res),
      catchError(this.handleErr)
    );
  }


  handleErr(error: HttpErrorResponse){
    let msg = '';
    if (error.error instanceof ErrorEvent){
      msg = error.message;
    } else {
      msg = `Error Status ; ${error.status}\nMessage ${error.message}`;
    }
    return throwError(error);
  }


  getArticle(slug: string): Observable<Article>{
    const articleUrl = `${this.url}/articles/${slug}`;
    return this.httpClient.get<Article>(articleUrl);
  }

  deleteArticle(article: Article): Observable<Article>{
    const url = `${this.url}/articles/${article.slug}`;
    return this.httpClient.delete<Article>(url, this.httpOption)
    .pipe(
      map(res => res),
      catchError(this.handleErr)
    );
  }

  favoriteArticle(article: Article): Observable<Article>{
    const favoriteUrl = `${this.url}/articles/${article.slug}/favorite`;
    return this.httpClient.post<Article>(favoriteUrl, {}, this.httpOption);
  }

  editArticle(slug){
    const url = `${this.url}/articles/${slug}`;
    return this.httpClient.get(url);
  }

  getTags(): Observable<Tags[]>{
    const url = `${this.url}/tags`;
    return this.httpClient.get<Tags[]>(url);

  }
}
