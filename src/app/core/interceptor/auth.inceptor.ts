import {Injectable } from '@angular/core';
import {HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import {AuthService } from '../service/auth.service';

@Injectable()

export class AuthInterceptor implements HttpInterceptor{
  constructor(private authService: AuthService){}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
     const authToken = this.authService.getToken();
     if (authToken){
      req = req.clone({
        setHeaders: {
          Authorization: authToken,
        }
      });
     }
     return next.handle(req);
  }
}


