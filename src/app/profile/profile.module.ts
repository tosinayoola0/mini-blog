import { NgModule } from '@angular/core';
import {ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../app-routing.module';
import {MaterialModule } from '../material/material.module';
import {HomeModule } from '../home/home.module';


@NgModule({
  declarations: [],
  imports: [
    AppRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HomeModule,
    MaterialModule,

  ],

})

export class ProfileModule { }

