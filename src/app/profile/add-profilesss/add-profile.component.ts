import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators } from '@angular/forms';
import {AuthService } from '../../core/service/auth.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Profile } from '../profile/data';

@Component({
  selector: 'app-add-profile',
  templateUrl: './add-profile.component.html',
  styleUrls: ['./add-profile.component.css']
})
export class AddProfileComponent implements OnInit {
  profile: Profile;
  profileForm: FormGroup;
  action: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthService,
              public route: Router,
              private activateRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.action = this.profile ? 'Add' : 'Update';
    this.profileForm = this.formBuilder.group({
      image: [''],
      bio: [''],
      username: ['']
    });
  }

  createProfile(){
    const username = this.activateRoute.snapshot.paramMap.get('username');
    this.authService.createProfile(this.profileForm.value, username)
      .subscribe(response => {
        this.route.navigate(['/index']);
      });
  }

}

