import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {AuthService } from '../../core/service/auth.service';
import { SignupDetail } from '../../models/userDetail';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;

  constructor(
              public fb: FormBuilder,
              private authService: AuthService,
              private route: Router,
    ){ }

  ngOnInit(): void {
    this.signupForm = this.fb.group({
      email: [
        '', [
            Validators.required, Validators.email
        ]
      ],
      password: [
        '',
        [
          Validators.required
        ]
      ],
      username: [
        '', [
          Validators.required
        ]
      ],
    });
  }

  userRegistration(){
    this.authService.userSignUp(this.signupForm.value).subscribe(
      response => {
        alert('registration successful');
        this.route.navigate(['login']);
      },
      error => {
        'registration error, try again';
      }
    );
  }
}
