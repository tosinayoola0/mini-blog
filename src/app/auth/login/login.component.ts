import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {AuthService } from '../../core/service/auth.service';
import { LoginDetail } from '../../models/userDetail';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private authSercive: AuthService,
    private fb: FormBuilder,
    ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      email: [
        '', [
          Validators.required, Validators.email
        ]
      ],
      password: [
        '', [
          Validators.required
        ]
      ],
    });
  }

  UserLogin(){
    this.authSercive.userLogin(this.loginForm.value);
  }
}

