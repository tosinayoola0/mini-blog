import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignupComponent } from './signup/signup.component';
import {ReactiveFormsModule, FormsModule } from '@angular/forms';
import {AppRoutingModule } from '../app-routing.module';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [SignupComponent, LoginComponent],

  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  exports: [
    LoginComponent,
    SignupComponent,
  ],
})
export class AuthModule { }
